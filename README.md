![logofinx](http://finxapp.com.br/wp-content/themes/finx/images/logo-finx.png)
# Teste de Front-end
Este teste é apresentado aos candidatos as vagas de desenvolvimento Front-end para avaliar os quesitos técnicos.

### O Desafio

Seu objetivo é criar um simples app que exibe um grid com os dados cadastrados e um formulário com os campos abaixo.

* Nome completo
* CPF
* Telefone
* Email

### Pré-requisitos: 
 - Utilizar Angular, ReactJS ou Vue.JS
 - A aplicação deve ser Mobile-First/Responsiva
 - Deve ser possível criar, listar e excluir os dados cadastrados pelo formulário;
 - Os inputs de texto e botão devem ter a aparência conforme o guia de estilo abaixo;
 - Fazer a persistência dos dados no `localStorage`;

Para ter o estado inicial da lista de usuário utilizar este recurso abaixo:

>     GET https://private-21e8de-rafaellucio.apiary-mock.com/users

modelo de Response da api:

```json

[
  {
    "name": "My name 1",
    "cpf": "04080757247",
    "phone": "11987654321",
    "email": "myemail1@test.com.br"
  },
  {
    "name": "My name 2",
    "cpf": "77797584192",
    "phone": "11987654321",
    "email": "myemail2@test.com.br"
  },
  {
    "name": "My name 3",
    "cpf": "45486737688",
    "phone": "11987654321",
    "email": "myemail3@test.com.br"
  }
]
```

A partir deste ponto utilizar o `localStorage/IndexedDB` para persistir localmente as informações.

Save:

```json
{
  "name": "My name 4",
  "cpf": "74668869066",
  "phone": "11987654321",
  "email": "myemail4@test.com.br"
}
```

Lista local:
```json
[
  {
    "name": "My name 1",
    "cpf": "04080757247",
    "phone": "11987654321",
    "email": "myemail1@test.com.br"
  },
  {
    "name": "My name 2",
    "cpf": "77797584192",
    "phone": "11987654321",
    "email": "myemail2@test.com.br"
  },
  {
    "name": "My name 3",
    "cpf": "45486737688",
    "phone": "11987654321",
    "email": "myemail3@test.com.br"
  },
  {
    "name": "My name 4",
    "cpf": "74668869066",
    "phone": "11987654321",
    "email": "myemail4@test.com.br"
  }
]
```

# Plus:
 1. Implementar funcionalidade EDITAR;
 2. Validações de erros
 3. Uso de SASS;

# O que esperamos:
 - Criar um passo a passo de como rodar sua aplicação.
 - Criar uma breve descrição da solução utilizada.

---
.
.


## Guia de estilo

### Input:
 - Cor da fonte sem foco: <span style="color:#efeeed">*#efeeed*</span>.
 - Cor da fonte com foco: <span style="color:#333333">*#333333*</span>.
 - Cor da borda: <span style="color:#efeeed">*#efeeed*</span>.

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/name.png?raw=true)

### Input Inválido:
 - Cor da fonte: <span style="color:#eb4a46">*#eb4a46*</span>.
 - Cor da borda: <span style="color:#eb4a46">*#eb4a46*</span>.

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/name_validation.png?raw=true)

### Botão Habilitado:
 - Cor da fonte com foco: <span style="color:#ffffff">*#ffffff*</span>.
 - Cor de background: <span style="color:#00c8b3">*#00c8b3*</span>.

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/button_enable.png?raw=true)

### Botão Hover:
 - Opacidade do botão com hover: 70%.

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/button_enable_hover.png?raw=true)

### Botão Desabilitado:
 - Cor da fonte sem foco: <span style="color:#dddcdc">*#dddcdc*</span>.
 - Cor de background: <span style="color:#f6f6f6">*#f6f6f6*</span>.

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/button_disable.png?raw=true)

### Botão Loading:

![inputs](https://github.com/easynvest/teste-front-end/blob/master/images/button_enable_loading.png?raw=true)

**Criar animação de loading ao clicar no submit*